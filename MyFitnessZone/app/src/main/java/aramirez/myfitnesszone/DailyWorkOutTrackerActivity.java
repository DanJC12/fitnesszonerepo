package aramirez.myfitnesszone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import aramirez.myfitnesszone.domain.WorkoutTracker;
import aramirez.myfitnesszone.service.IWorkoutTrackerSvc;
import aramirez.myfitnesszone.service.WorkoutTrackerSvcSioImpl;

public class DailyWorkOutTrackerActivity extends Activity implements AdapterView.OnItemSelectedListener {

    private Context context = null;
    private IWorkoutTrackerSvc impl = null;
    private WorkoutTracker workoutTable = null;
    Spinner spinner1;
    String spin1 = "";
    EditText row31;
    EditText row32;
    EditText row33;
    EditText row34;
    Spinner spinner2;
    String spin2 = "";
    EditText row41;
    EditText row42;
    EditText row43;
    EditText row44;
    Spinner spinner3;
    String spin3 = "";
    EditText row51;
    EditText row52;
    EditText row53;
    EditText row54;
    Spinner spinner4;
    String spin4 = "";
    EditText row61;
    EditText row62;
    EditText row63;
    EditText row64;
    Spinner spinner5;
    String spin5 = "";
    EditText row71;
    EditText row72;
    EditText row73;
    EditText row74;
    Spinner spinner6;
    String spin6 = "";
    EditText row81;
    EditText row82;
    EditText row83;
    EditText row84;
    Spinner spinner7;
    String spin7 = "";
    EditText row91;
    EditText row92;
    EditText row93;
    EditText row94;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_work_out_tracker);

        //Get Instance
        context = this;
        impl = WorkoutTrackerSvcSioImpl.getInstance(context);
        workoutTable = impl.retrieve();

        // Get the Today's Date
        Calendar cal = Calendar.getInstance();
        String strDate = cal.getTime() + "";
        String day = strDate.split(" ")[0];
        String month = strDate.split(" ")[1];
        String dayNum = strDate.split(" ")[2];
        strDate = day + " " + month + " " + dayNum;
        TextView dateText = (TextView) findViewById(R.id.textViewDate2);
        dateText.setText(strDate);

        // Get the Weight if it was entered in the Fitness Journal
        Intent intent = getIntent();
        String message = intent.getStringExtra("Weight");
        TextView textViewWeight = (TextView) findViewById(R.id.TextViewWeight2);
        textViewWeight.setText(message);

        // Setup Row 1
        spinner1 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(this);
        if (!workoutTable.getRow1().get(0).toString().equals("")) {
            int spinner1Position = adapter1.getPosition(workoutTable.getRow1().get(0).toString());
            spinner1.setSelection(spinner1Position);
        }

        row31 = (EditText) findViewById(R.id.editTextRow31);
        row31.setText(workoutTable.getRow1().get(1).toString());
        row32 = (EditText) findViewById(R.id.editTextRow32);
        row32.setText(workoutTable.getRow1().get(2).toString());
        row33 = (EditText) findViewById(R.id.editTextRow33);
        row33.setText(workoutTable.getRow1().get(3).toString());
        row34 = (EditText) findViewById(R.id.editTextRow34);
        row34.setText(workoutTable.getRow1().get(4).toString());

        // Setup Row 2
        spinner2 = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(this);
        if (!workoutTable.getRow2().get(0).toString().equals("")) {
            int spinner2Position = adapter2.getPosition(workoutTable.getRow2().get(0).toString());
            spinner2.setSelection(spinner2Position);
        }

        row41 = (EditText) findViewById(R.id.editTextRow41);
        row41.setText(workoutTable.getRow2().get(1).toString());
        row42 = (EditText) findViewById(R.id.editTextRow42);
        row42.setText(workoutTable.getRow2().get(2).toString());
        row43 = (EditText) findViewById(R.id.editTextRow43);
        row43.setText(workoutTable.getRow2().get(3).toString());
        row44 = (EditText) findViewById(R.id.editTextRow44);
        row44.setText(workoutTable.getRow2().get(4).toString());

        // Setup Row 3
        spinner3 = (Spinner) findViewById(R.id.spinner4);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapter3);
        spinner3.setOnItemSelectedListener(this);
        if (!workoutTable.getRow3().get(0).toString().equals("")) {
            int spinner3Position = adapter3.getPosition(workoutTable.getRow3().get(0).toString());
            spinner3.setSelection(spinner3Position);
            spinner3.setSelection(spinner3Position);
        }

        row51 = (EditText) findViewById(R.id.editTextRow51);
        row51.setText(workoutTable.getRow3().get(1).toString());
        row52 = (EditText) findViewById(R.id.editTextRow52);
        row52.setText(workoutTable.getRow3().get(2).toString());
        row53 = (EditText) findViewById(R.id.editTextRow53);
        row53.setText(workoutTable.getRow3().get(3).toString());
        row54 = (EditText) findViewById(R.id.editTextRow54);
        row54.setText(workoutTable.getRow3().get(4).toString());

        // Setup Row 4
        spinner4 = (Spinner) findViewById(R.id.spinner5);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner4.setAdapter(adapter4);
        spinner4.setOnItemSelectedListener(this);
        if (!workoutTable.getRow4().get(0).toString().equals("")) {
            int spinner4Position = adapter4.getPosition(workoutTable.getRow4().get(0).toString());
            spinner4.setSelection(spinner4Position);
        }

        row61 = (EditText) findViewById(R.id.editTextRow61);
        row61.setText(workoutTable.getRow4().get(1).toString());
        row62 = (EditText) findViewById(R.id.editTextRow62);
        row62.setText(workoutTable.getRow4().get(2).toString());
        row63 = (EditText) findViewById(R.id.editTextRow63);
        row63.setText(workoutTable.getRow4().get(3).toString());
        row64 = (EditText) findViewById(R.id.editTextRow64);
        row64.setText(workoutTable.getRow4().get(4).toString());

        // Setup Row 5
        spinner5 = (Spinner) findViewById(R.id.spinner6);
        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner5.setAdapter(adapter5);
        spinner5.setOnItemSelectedListener(this);
        if (!workoutTable.getRow5().get(0).toString().equals("")) {
            int spinner5Position = adapter5.getPosition(workoutTable.getRow5().get(0).toString());
            spinner5.setSelection(spinner5Position);
        }

        row71 = (EditText) findViewById(R.id.editTextRow71);
        row71.setText(workoutTable.getRow5().get(1).toString());
        row72 = (EditText) findViewById(R.id.editTextRow72);
        row72.setText(workoutTable.getRow5().get(2).toString());
        row73 = (EditText) findViewById(R.id.editTextRow73);
        row73.setText(workoutTable.getRow5().get(3).toString());
        row74 = (EditText) findViewById(R.id.editTextRow74);
        row74.setText(workoutTable.getRow5().get(4).toString());

        // Setup Row 6
        spinner6 = (Spinner) findViewById(R.id.spinner7);
        ArrayAdapter<CharSequence> adapter6 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner6.setAdapter(adapter6);
        spinner6.setOnItemSelectedListener(this);
        if (!workoutTable.getRow6().get(0).toString().equals("")) {
            int spinner6Position = adapter6.getPosition(workoutTable.getRow6().get(0).toString());
            spinner6.setSelection(spinner6Position);
        }

        row81 = (EditText) findViewById(R.id.editTextRow81);
        row81.setText(workoutTable.getRow6().get(1).toString());
        row82 = (EditText) findViewById(R.id.editTextRow82);
        row82.setText(workoutTable.getRow6().get(2).toString());
        row83 = (EditText) findViewById(R.id.editTextRow83);
        row83.setText(workoutTable.getRow6().get(3).toString());
        row84 = (EditText) findViewById(R.id.editTextRow84);
        row84.setText(workoutTable.getRow6().get(4).toString());

        // Setup Row 7
        spinner7 = (Spinner) findViewById(R.id.spinner8);
        ArrayAdapter<CharSequence> adapter7 = ArrayAdapter.createFromResource(this, R.array.exercise_list, android.R.layout.simple_spinner_item);
        adapter7.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner7.setAdapter(adapter7);
        spinner7.setOnItemSelectedListener(this);
        if (!workoutTable.getRow7().get(0).toString().equals("")) {
            int spinner7Position = adapter7.getPosition(workoutTable.getRow7().get(0).toString());
            spinner7.setSelection(spinner7Position);
        }

        row91 = (EditText) findViewById(R.id.editTextRow91);
        row91.setText(workoutTable.getRow7().get(1).toString());
        row92 = (EditText) findViewById(R.id.editTextRow92);
        row92.setText(workoutTable.getRow7().get(2).toString());
        row93 = (EditText) findViewById(R.id.editTextRow93);
        row93.setText(workoutTable.getRow7().get(3).toString());
        row94 = (EditText) findViewById(R.id.editTextRow94);
        row94.setText(workoutTable.getRow7().get(4).toString());

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        /* Use the following switch-statement if you want to keep all spinner actions/callbacks together */
        /* The same can be done to the onNothingSelected callback */
        switch (parent.getId()) {
            case R.id.spinner2:
                spin1 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner3:
                spin2 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner4:
                spin3 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner5:
                spin4 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner6:
                spin5 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner7:
                spin6 = (String) parent.getItemAtPosition(position);
                break;
            case R.id.spinner8:
                spin7 = (String) parent.getItemAtPosition(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void save(View view) {

        ArrayList newrow1 = new ArrayList(Arrays.asList(spin1, row31.getText().toString(), row32.getText().toString(), row33.getText().toString(), row34.getText().toString()));
        ArrayList newrow2 = new ArrayList(Arrays.asList(spin2, row41.getText().toString(), row42.getText().toString(), row43.getText().toString(), row44.getText().toString()));
        ArrayList newrow3 = new ArrayList(Arrays.asList(spin3, row51.getText().toString(), row52.getText().toString(), row53.getText().toString(), row54.getText().toString()));
        ArrayList newrow4 = new ArrayList(Arrays.asList(spin4, row61.getText().toString(), row62.getText().toString(), row63.getText().toString(), row64.getText().toString()));
        ArrayList newrow5 = new ArrayList(Arrays.asList(spin5, row71.getText().toString(), row72.getText().toString(), row73.getText().toString(), row74.getText().toString()));
        ArrayList newrow6 = new ArrayList(Arrays.asList(spin6, row81.getText().toString(), row82.getText().toString(), row83.getText().toString(), row84.getText().toString()));
        ArrayList newrow7 = new ArrayList(Arrays.asList(spin7, row91.getText().toString(), row92.getText().toString(), row93.getText().toString(), row94.getText().toString()));

        WorkoutTracker updatedWorkoutTracker = new WorkoutTracker();
        updatedWorkoutTracker.setRow1(newrow1);
        updatedWorkoutTracker.setRow2(newrow2);
        updatedWorkoutTracker.setRow3(newrow3);
        updatedWorkoutTracker.setRow4(newrow4);
        updatedWorkoutTracker.setRow5(newrow5);
        updatedWorkoutTracker.setRow6(newrow6);
        updatedWorkoutTracker.setRow7(newrow7);

        impl.update(updatedWorkoutTracker);
        Context context = getApplicationContext();
        CharSequence text = "Content has been saved.";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void delete(View view) {
        impl.clear();
        Intent intent = new Intent(this, DailyWorkOutTrackerActivity.class);
        finish();
        startActivity(intent);
    }


}
