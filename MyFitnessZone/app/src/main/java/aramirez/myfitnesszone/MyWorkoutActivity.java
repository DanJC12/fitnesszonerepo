package aramirez.myfitnesszone;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

public class MyWorkoutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_workout);
    }

    public void cardio (View view){
        Intent intent = new Intent(this, CardioActivity.class);
        startActivity(intent);

    }

    public void arm (View view){
        Intent intent = new Intent(this, ArmActivity.class);
        startActivity(intent);

    }

    public void leg (View view){
        Intent intent = new Intent(this, LegActivity.class);
        startActivity(intent);

    }


    public void stretch (View view){
        Intent intent = new Intent(this, StretchesActivity.class);
        startActivity(intent);

    }

    public void fitnessJournal (View view){
        Intent intent = new Intent(this, FitnessJournalActivity.class);
        startActivity(intent);

    }

}
