package aramirez.myfitnesszone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import aramirez.myfitnesszone.exercises.BalanceBoard;
import aramirez.myfitnesszone.exercises.BalanceBoardPushUps;
import aramirez.myfitnesszone.exercises.BalanceBoardSquats;
import aramirez.myfitnesszone.exercises.CaptainChair;
import aramirez.myfitnesszone.exercises.CardioWalker;
import aramirez.myfitnesszone.exercises.ChestPress;
import aramirez.myfitnesszone.exercises.ChinUp;
import aramirez.myfitnesszone.exercises.Elliptical;
import aramirez.myfitnesszone.exercises.HorizontalChinUp;
import aramirez.myfitnesszone.exercises.LegExtension;
import aramirez.myfitnesszone.exercises.PbBenchDips;
import aramirez.myfitnesszone.exercises.PbBoxJumps;
import aramirez.myfitnesszone.exercises.PbBoxPushUps;
import aramirez.myfitnesszone.exercises.PushUps;

public class FitnessEquipmentBenefitsChart extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_equipment_benefits_chart);
    }

    public void balanceBoard (View view) {
        Intent intent = new Intent(this, BalanceBoard.class);
        startActivity(intent);
    }

    public void balanceBoardPushUps (View view) {
        Intent intent = new Intent(this, BalanceBoardPushUps.class);
        startActivity(intent);
    }

    public void balanceBoardSquats (View view) {
        Intent intent = new Intent(this, BalanceBoardSquats.class);
        startActivity(intent);
    }

    public void captainChair (View view) {
        Intent intent = new Intent(this, CaptainChair.class);
        startActivity(intent);
    }

    public void cardioWalker (View view) {
        Intent intent = new Intent(this, CardioWalker.class);
        startActivity(intent);

    }

    public void chestPress (View view) {
        Intent intent = new Intent(this, ChestPress.class);
        startActivity(intent);

    }

    public void chinUp (View view) {
        Intent intent = new Intent(this, ChinUp.class);
        startActivity(intent);

    }

    public void elliptical (View view) {
        Intent intent = new Intent(this, Elliptical.class);
        startActivity(intent);

    }

    public void horizaontalChinUp (View view) {
        Intent intent = new Intent(this, HorizontalChinUp.class);
        startActivity(intent);

    }

    public void legExtension (View view) {
        Intent intent = new Intent(this, LegExtension.class);
        startActivity(intent);

    }

    public void pbBenchDips (View view) {
        Intent intent = new Intent(this, PbBenchDips.class);
        startActivity(intent);

    }

    public void pbBoxJumps (View view) {
        Intent intent = new Intent(this, PbBoxJumps. class);
        startActivity(intent);

    }

    public void pbBoxPushUps (View view) {
        Intent intent = new Intent(this, PbBoxPushUps.class);
        startActivity(intent);

    }

    public void pushup (View view) {
        Intent intent = new Intent(this, PushUps. class);
        startActivity(intent);

    }


}
