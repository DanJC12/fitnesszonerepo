package aramirez.myfitnesszone.service;

import android.database.Cursor;

import java.util.List;

import aramirez.myfitnesszone.domain.JournalEntry;

/**
 * Created by alexr_000 on 11/6/2016.
 */

public interface IJournalEntrySvc {

    JournalEntry retrieve(); //called @ app start

    void clear();

    void update(JournalEntry updatedEntry);
}
