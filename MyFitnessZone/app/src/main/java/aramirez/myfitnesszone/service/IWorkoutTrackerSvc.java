package aramirez.myfitnesszone.service;

import aramirez.myfitnesszone.domain.WorkoutTracker;

/**
 * Created by alexr_000 on 11/10/2016.
 */

public interface IWorkoutTrackerSvc {

    WorkoutTracker retrieve(); //called @ app start

    void clear();

    void update(WorkoutTracker updatedWorkout);
}
