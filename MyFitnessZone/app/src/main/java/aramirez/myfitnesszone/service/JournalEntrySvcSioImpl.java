package aramirez.myfitnesszone.service;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import aramirez.myfitnesszone.domain.JournalEntry;


public class JournalEntrySvcSioImpl implements IJournalEntrySvc {


    private final static String TAG = "JournalEntrySvcSioImpl";

    private Context context = null;

    private JournalEntry theEntry = new JournalEntry();

    private final static String FILENAME = "journalentry.sio";

    private static JournalEntrySvcSioImpl instance = null;

    private JournalEntrySvcSioImpl() {

    }

    private JournalEntrySvcSioImpl(Context context) {

        this.context = context;
    }

    public static JournalEntrySvcSioImpl getInstance(Context context) {

        if (instance == null) {
            instance = new JournalEntrySvcSioImpl(context);
        }

        return instance;
    }


    public JournalEntry retrieve() {
        //called @ app start
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            theEntry = (JournalEntry) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception e) {
            Log.i(TAG, "Exception" + e.getMessage());
        }
        return theEntry;
    }

    public void clear() {

        theEntry.setWeight("");
        theEntry.setWorkoutsTracked("");
        theEntry.setGoal("");
        theEntry.setMonday("");
        theEntry.setTuesday("");
        theEntry.setWednesday("");
        theEntry.setThursday("");
        theEntry.setFriday("");
        theEntry.setSaturday("");
        theEntry.setSunday("");
        writeFile();
    }

    public void update(JournalEntry updatedEntry) {

        theEntry = updatedEntry;
        writeFile();

    }

    public void writeFile(){
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(theEntry);
            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.i(TAG, "Exception" + e.getMessage());
        }


    }

}
