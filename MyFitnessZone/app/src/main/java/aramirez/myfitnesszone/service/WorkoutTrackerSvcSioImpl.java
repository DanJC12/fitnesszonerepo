package aramirez.myfitnesszone.service;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import aramirez.myfitnesszone.domain.WorkoutTracker;

/**
 * Created by alexr_000 on 11/10/2016.
 */

public class WorkoutTrackerSvcSioImpl implements IWorkoutTrackerSvc {

    private final static String TAG = "WorkoutTracker...Impl";

    private Context context = null;

    private WorkoutTracker workoutTracker = new WorkoutTracker();

    private final static String FILENAME = "workoutTracker.sio";

    private static WorkoutTrackerSvcSioImpl instance = null;


    private WorkoutTrackerSvcSioImpl() {

    }

    private WorkoutTrackerSvcSioImpl(Context context) {

        this.context = context;
    }

    public static WorkoutTrackerSvcSioImpl getInstance(Context context) {

        if (instance == null) {
            instance = new WorkoutTrackerSvcSioImpl(context);
        }

        return instance;
    }


    public WorkoutTracker retrieve() {
        //called @ app start
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            workoutTracker = (WorkoutTracker) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception e) {
            Log.i(TAG, "Exception" + e.getMessage());
        }
        return workoutTracker;
    }

    public void clear() {

        ArrayList newrow1 = workoutTracker.getRow1();
        newrow1.set(0,"");
        newrow1.set(1,"");
        newrow1.set(2,"");
        newrow1.set(3,"");
        newrow1.set(4,"");
        ArrayList newrow2 = workoutTracker.getRow2();
        newrow2.set(0,"");
        newrow2.set(1,"");
        newrow2.set(2,"");
        newrow2.set(3,"");
        newrow2.set(4,"");
        ArrayList newrow3 = workoutTracker.getRow3();
        newrow3.set(0,"");
        newrow3.set(1,"");
        newrow3.set(2,"");
        newrow3.set(3,"");
        newrow3.set(4,"");
        ArrayList newrow4 = workoutTracker.getRow4();
        newrow4.set(0,"");
        newrow4.set(1,"");
        newrow4.set(2,"");
        newrow4.set(3,"");
        newrow4.set(4,"");
        ArrayList newrow5 = workoutTracker.getRow5();
        newrow5.set(0,"");
        newrow5.set(1,"");
        newrow5.set(2,"");
        newrow5.set(3,"");
        newrow5.set(4,"");
        ArrayList newrow6 = workoutTracker.getRow6();
        newrow6.set(0,"");
        newrow6.set(1,"");
        newrow6.set(2,"");
        newrow6.set(3,"");
        newrow6.set(4,"");
        ArrayList newrow7 = workoutTracker.getRow7();
        newrow7.set(0,"");
        newrow7.set(1,"");
        newrow7.set(2,"");
        newrow7.set(3,"");
        newrow7.set(4,"");

        workoutTracker.setRow1(newrow1);
        workoutTracker.setRow2(newrow2);
        workoutTracker.setRow3(newrow3);
        workoutTracker.setRow4(newrow4);
        workoutTracker.setRow5(newrow5);
        workoutTracker.setRow6(newrow6);
        workoutTracker.setRow7(newrow7);

        writeFile();
    }

    public void update(WorkoutTracker updatedTracker) {

        workoutTracker = updatedTracker;
        writeFile();

    }

    public void writeFile() {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(workoutTracker);
            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.i(TAG, "Exception" + e.getMessage());
        }


    }

}


