package aramirez.myfitnesszone;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void openTPL (View view){
        Intent opentpl = new Intent(Intent.ACTION_VIEW, Uri.parse("http://tpl.org"));
        startActivity(opentpl);

    }

    public void openRU (View view){
        Intent openru = new Intent(Intent.ACTION_VIEW, Uri.parse("http://regis.edu"));
        startActivity(openru);
    }

    public void openCH(View view){
        Intent opench = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.regis.edu/Community-Gateway/Cultivate-Health.aspx"));
        startActivity(opench);
    }

}
