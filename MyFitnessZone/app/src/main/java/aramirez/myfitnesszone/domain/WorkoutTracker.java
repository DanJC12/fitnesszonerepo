package aramirez.myfitnesszone.domain;

import java.io.Serializable;
import java.util.*;

/**
 * Created by alexr_000 on 11/10/2016.
 */

public class WorkoutTracker implements Serializable {

    private ArrayList row1 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row2 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row3 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row4 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row5 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row6 = new ArrayList(Arrays.asList("", "", "", "", ""));
    private ArrayList row7 = new ArrayList(Arrays.asList("", "", "", "", ""));

    public ArrayList getRow1() {
        return row1;
    }

    public void setRow1(ArrayList row1) {
        this.row1 = row1;
    }

    public ArrayList getRow2() {
        return row2;
    }

    public void setRow2(ArrayList row2) {
        this.row2 = row2;
    }

    public ArrayList getRow3() {
        return row3;
    }

    public void setRow3(ArrayList row3) {
        this.row3 = row3;
    }

    public ArrayList getRow4() {
        return row4;
    }

    public void setRow4(ArrayList row4) {
        this.row4 = row4;
    }

    public ArrayList getRow5() {
        return row5;
    }

    public void setRow5(ArrayList row5) {
        this.row5 = row5;
    }

    public ArrayList getRow6() {
        return row6;
    }

    public void setRow6(ArrayList row6) {
        this.row6 = row6;
    }

    public ArrayList getRow7() {
        return row7;
    }

    public void setRow7(ArrayList row7) {
        this.row7 = row7;
    }
}
