package aramirez.myfitnesszone.domain;

import java.io.Serializable;

public class JournalEntry implements Serializable {

    private String weight = "";
    private String workoutsTracked = "";
    private String goal = "";
    private String monday = "";
    private String tuesday = "";
    private String wednesday = "";
    private String thursday = "";
    private String friday = "";
    private String saturday = "";
    private String sunday = "";


    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWorkoutsTracked() {
        return workoutsTracked;
    }

    public void setWorkoutsTracked(String workoutsTracked) {
        this.workoutsTracked = workoutsTracked;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

}
