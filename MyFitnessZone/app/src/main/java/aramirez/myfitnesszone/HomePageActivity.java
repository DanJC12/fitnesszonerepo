package aramirez.myfitnesszone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class HomePageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.aboutApp){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void findLocation (View view){
        Intent intent = new Intent(this,FindLocationActivity.class);
        startActivity(intent);
    }

    public void healthBen (View view){
        Intent intent = new Intent(this, HealthBenActivity.class);
        startActivity(intent);
    }

    public void fitnessJournal (View view) {
        Intent intent = new Intent(this, FitnessJournalActivity.class);
        startActivity(intent);
    }

    public void myWorkout (View view){
        Intent intent = new Intent(this, MyWorkoutActivity.class);
        startActivity(intent);
    }

    public void fitnessEquipment(View view) {
        Intent intent = new Intent(this, FitnessEquipmentBenefitsChart.class);
        startActivity(intent);
    }

    public void about(MenuItem item) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void settings (MenuItem item) {
        Intent intent  = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
