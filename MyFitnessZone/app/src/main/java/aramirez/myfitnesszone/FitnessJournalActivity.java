package aramirez.myfitnesszone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import aramirez.myfitnesszone.domain.JournalEntry;
import aramirez.myfitnesszone.service.IJournalEntrySvc;
import aramirez.myfitnesszone.service.JournalEntrySvcSioImpl;

public class FitnessJournalActivity extends Activity {

    private static final String TAG = "FitnessJournalActivity";

    private Context context = null;
    private IJournalEntrySvc impl = null;
    private JournalEntry journalEntry = null;
    EditText goal;
    EditText weight;
    EditText workoutsTracked;
    EditText monday;
    EditText tuesday;
    EditText wednesday;
    EditText thursday;
    EditText friday;
    EditText saturday;
    EditText sunday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_journal);

        //Get Instance
        context = this;
        impl = JournalEntrySvcSioImpl.getInstance(context);
        journalEntry = impl.retrieve();

        // Setup handles on EditText objects and set their strings
        goal = (EditText) findViewById(R.id.editTextGoal);
        goal.setText(journalEntry.getGoal());
        monday = (EditText) findViewById(R.id.editText1);
        monday.setText(journalEntry.getMonday());
        tuesday = (EditText) findViewById(R.id.editText2);
        tuesday.setText(journalEntry.getTuesday());
        wednesday = (EditText) findViewById(R.id.editText3);
        wednesday.setText(journalEntry.getWednesday());
        thursday = (EditText) findViewById(R.id.editText4);
        thursday.setText(journalEntry.getThursday());
        friday = (EditText) findViewById(R.id.editText5);
        friday.setText(journalEntry.getFriday());
        saturday = (EditText) findViewById(R.id.editText6);
        saturday.setText(journalEntry.getSaturday());
        sunday = (EditText) findViewById(R.id.editText7);
        sunday.setText(journalEntry.getSunday());
        weight = (EditText) findViewById(R.id.editTextWeight);
        weight.setText(journalEntry.getWeight());
        workoutsTracked = (EditText) findViewById(R.id.editTextTrack);
        workoutsTracked.setText(journalEntry.getWorkoutsTracked());
    }


    public void daily(View view) {
        Intent intent = new Intent(this, DailyWorkOutTrackerActivity.class);
        String message = weight.getText().toString();
        intent.putExtra("Weight", message);
        startActivity(intent);
    }

    public void save(View view) {

        JournalEntry updatedEntry = new JournalEntry();
        updatedEntry.setGoal(goal.getText().toString());
        updatedEntry.setWeight(weight.getText().toString());
        updatedEntry.setWorkoutsTracked(workoutsTracked.getText().toString());
        updatedEntry.setMonday(monday.getText().toString());
        updatedEntry.setTuesday(tuesday.getText().toString());
        updatedEntry.setWednesday(wednesday.getText().toString());
        updatedEntry.setThursday(thursday.getText().toString());
        updatedEntry.setFriday(friday.getText().toString());
        updatedEntry.setSaturday(saturday.getText().toString());
        updatedEntry.setSunday(sunday.getText().toString());
        impl.update(updatedEntry);
        Context context = getApplicationContext();
        CharSequence text = "Content has been saved.";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();


    }

    public void delete(View view) {

        impl.clear();
        Intent intent = new Intent(this, FitnessJournalActivity.class);
        finish();
        startActivity(intent);

    }

}
